$("#sehirler").change(function() {
  var sehir = $("#sehirler option:selected");
  $("#loading").show();
  $("#sonuclar").html("");
  $.ajax({
    method : "GET",
    url : "http://api.openweathermap.org/data/2.5/weather?q="+ sehir.val() +",&lang=tr&appid=c434d8df96f8d0250cbad699fe2f2a01"
  }).then(function (response){
    switch (response.weather[0].main) {
      case "Rain":
          $("#sounds").html("<audio " + "autoplay>"+
                "<source src='./sounds/rain-water-01.wav'>"+
              "</audio>");
        break;
      case "Clear":
          $("#sounds").html("<audio " + "autoplay>"+
                "<source src='./sounds/marti-sesi.mp3'>"+
              "</audio>");
        break;
        case "Clouds":
            $("#sounds").html("<audio " + "autoplay>"+
                  "<source src='./sounds/windchimes-01.wav'>"+
                "</audio>");
          break;
        case "Wind":
            $("#sounds").html("<audio " + "autoplay>"+
                  "<source src='./sounds/wind-01.wav'>"+
                "</audio>");
        break;


    }
    $("#sonuclar").append("<li>" + response.weather[0].description + "</li>");
    $("#loading").hide();
  });
});
